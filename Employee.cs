using System;
using System.Collections.Generic;

namespace EmployeeManager
{
    
       public class Employee
   {   
       
       public string firstName{get; set;}
       public string lastName{get; set;}
       public string email{get; set;}
       public string gender{get; set;}
       public int age{get; set;}
       public double salary{get; set;}
       public string employeeType{get; set;}

      public Employee(string firstName,string lastName,string email,string gender,int age,double salary,string employeeType)
      {
          this.firstName=firstName;
          this.lastName=lastName;
          this.email=email;
          this.gender=gender;
          this.age=age;
          this.salary=salary;
          this.employeeType=employeeType;
      } 

   }
    


}
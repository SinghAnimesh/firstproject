﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeManager
{
    public delegate void IsForDetail(List<Employee> employeeList);

    class DataManager
    {
        static string detailByEmail=string.Empty;
        static void Main(string[] args)
        {
            var employeeList=new List <Employee>();
            DisplayFormat(employeeList);
            
        }
        public  enum Gender
    {
           Male,
           Female,
           Other
    }

    public enum EmployeeType
    {
        PermanentEmployee,
        ContractualEmployee
    }

        public static Employee AddEmployee(List<Employee> employeeList)
        {
            Console.WriteLine("Please enter First Name");
            string firstName = Console.ReadLine();
            firstName = FirstNameValid(firstName);
            Console.WriteLine("Please enter Last Name");
            string lastName = Console.ReadLine();
            lastName = LastNameValid(lastName);
            Console.WriteLine("Please enter Email address");
            string email = Console.ReadLine();
            email = EmailValidation(employeeList, email);
            Console.WriteLine("Please enter Gender\nEnter 0 for Male\nEnter 1 for Female\nEnter 2 for Other");
            int genderDecider = Convert.ToInt32(Console.ReadLine());
            string gender = GenderDecider(genderDecider);
            GenderValid(ref genderDecider, ref gender);
            Console.WriteLine("Please enter Age");
            int age = Convert.ToInt32(Console.ReadLine());
            age = AgeValid(age);
            Console.WriteLine("Please enter Salary");
            double salary = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Please enter Employee Type\nEnter 0 for PermanentEmployee\nEnter 1 for ContractualEmployee");
            int employeeTypeDecider = Convert.ToInt32(Console.ReadLine());
            string employeeType = EmployeeTypeDecider(employeeTypeDecider);
            EmployeeTypeValid(ref employeeTypeDecider, ref employeeType);
            return new Employee(firstName, lastName, email, gender, age, salary, employeeType);
        }

        private static int AgeValid(int age)
        {
            while (age < 0)
            {
                Console.WriteLine("Please enter valid Age");
                age = Convert.ToInt32(Console.ReadLine());
            }

            return age;
        }

        private static string EmailValidation(List<Employee> employeeList, string email)
        {
            while (employeeList.Any(r => r.email == email))
            {
                Console.WriteLine("This email is already in record. Please enter another Email address");
                email = Console.ReadLine();
            }
            while (!email.Contains('@'))
            {
                Console.WriteLine("Please enter valid email address");
                email = Console.ReadLine();
            }

            return email;
        }

        private static string LastNameValid(string lastName)
        {
            while (lastName.Length < 2 || lastName.Length > 20)
            {
                Console.WriteLine("Please enter valid Last Name ranges between 2-20 character");
                lastName = Console.ReadLine();
            }

            return lastName;
        }

        private static string FirstNameValid(string firstName)
        {
            while (firstName.Length < 2 || firstName.Length > 20)
            {
                Console.WriteLine("Please enter valid First Name ranges between 2-20 character");
                firstName = Console.ReadLine();
            }

            return firstName;
        }
        private static void EmployeeTypeValid(ref int employeeTypeDecider,ref  string employeeType)
            {
                while (employeeType.Equals("invalid"))
                {
                    Console.WriteLine("Invalid for Employee type");
                    Console.WriteLine("Please enter Employee Type\nEnter 0 for PermanentEmployee\nEnter 1 for ContractualEmployee");
                    employeeTypeDecider = Convert.ToInt32(Console.ReadLine());
                    employeeType = EmployeeTypeDecider(employeeTypeDecider);
                }
            }
        private static void GenderValid(ref int genderDecider,ref string gender)
            {
                while (gender.Equals("invalid"))
                {
                    Console.WriteLine("Invalid for Gender");
                    Console.WriteLine("Please enter Gender\nEnter 0 for Male\nEnter 1 for Female\nEnter 2 for Other");
                    genderDecider = Convert.ToInt32(Console.ReadLine());
                    gender = GenderDecider(genderDecider);
                }
            }


        private static string EmployeeTypeDecider(int employeeTypeDecider)
        {
            
            switch(employeeTypeDecider)
            {
                case (int)EmployeeType.PermanentEmployee:
                return "PermanentEmployee";
                
                case (int)EmployeeType.ContractualEmployee:
                return "ContractualEmployee";
                
                default:
                return "invalid";
                
            }
            
        }
        private static string GenderDecider(int gen)
        {
            
            switch(gen)
            {
                case (int)Gender.Male:
                return "Male";
                
                case (int)Gender.Female:
                return"Female";

                case (int)Gender.Other:
                return "Other";

                default:
                return "invalid";
            }
            
        }

        public static bool Delete(List<Employee> employeeList,string deleting)
        {
            var emp=employeeList.Find(x=>x.email==deleting);
            return employeeList.Remove(emp); 
        }
        
        public static bool Delete(List<Employee> employeeList,string deleting,bool flag)
        {
            int count=0;
            count=employeeList.RemoveAll(x=>x.firstName==deleting);
            if(count!=0)
            return flag=true;
            return flag;
        }
        public static bool UpdateEmployee(List<Employee> employeeList,string email)
        {
            if(!employeeList.Any(x=>x.email==email))
            {
                return false;
            }
            else
            {
            var emp=employeeList.Find(x=>x.email==email);
            Console.WriteLine($"Please enter first name to update, existing first name is {emp.firstName}");
            emp.firstName=Console.ReadLine();
            emp.firstName = FirstNameValid(emp.firstName);
            Console.WriteLine($"Please enter last name to update, existing last name is {emp.lastName}");
            emp.lastName=Console.ReadLine();
            emp.lastName = LastNameValid(emp.lastName);
            Console.WriteLine($"Please enter Gender to update, existing Gender is {emp.gender}\nEnter 0 for Male\nEnter 1 for Female\nEnter 2 for Other");
            int genderDecider = Convert.ToInt32(Console.ReadLine());
            emp.gender=GenderDecider(genderDecider);
            string gen=emp.gender;
            GenderValid( ref genderDecider, ref gen);
            emp.gender=gen;
            Console.WriteLine($"Please enter Age to update, existing age is {emp.age}");
            emp.age = Convert.ToInt32(Console.ReadLine());
            emp.age = AgeValid(emp.age);
            Console.WriteLine($"Please enter Salary to update, existinf salary is {emp.salary}");
            emp.salary = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Please enter Employee Type to update, existing type is {emp.employeeType}\nEnter 0 for PermanentEmployee\nEnter 1 for ContractualEmployee");
            int employeeTypeDecider = Convert.ToInt32(Console.ReadLine());
            emp.employeeType = EmployeeTypeDecider(employeeTypeDecider);
            string empType=emp.employeeType;
            EmployeeTypeValid(ref employeeTypeDecider,ref empType);
            emp.employeeType=empType;
            return true;
            }
        }
        public static void AllDetails(List<Employee> employeeList)
        {
            foreach (var employee in employeeList)
                    {
                        Console.WriteLine($"{employee.firstName} {employee.lastName} {employee.email} {employee.gender} {employee.age} {employee.salary} {employee.employeeType}");
                    }
                    Console.WriteLine("\n\n");
        }

        public static void DetailsByEmail(List<Employee> employeeList)
        {
            var employeeLists=(from emp in employeeList
                                where emp.email==detailByEmail
                                select emp).ToList();
            if(employeeLists.Count>0)
            {
                foreach(var employee in employeeLists)
                {
                Console.WriteLine($"{employee.firstName} {employee.lastName} {employee.email} {employee.gender} {employee.age} {employee.salary} {employee.employeeType}\n\n");
                }

            }
            else Console.WriteLine("No record with this email.\n\n");
            
        }

        public static bool TotalSalary(List<Employee> employeeList,string email)
        {
            if(!employeeList.Any(x=>x.email==email))
            {
                return false;
            }
            else
            {
            var emp=employeeList.Find(x=>x.email==email);
            if(emp.employeeType.Equals("PermanentEmployee"))
            {
                Console.WriteLine($"{(emp.salary*12)+(emp.salary*0.08)}");
            }
            else
            Console.WriteLine($"{(emp.salary*12)}");
            return true;
            }
        }
        public static void DisplayFormat(List<Employee> employeeList)
        {
            Console.WriteLine("Please select the option\nPress 1 to add an employee\nPress 2 to get details of all the employees\nPress 3 to update an employee ");
            Console.WriteLine("Press 4 to delete an employee by entering Employee email address");
            Console.WriteLine("Press 5 to delete employees by entering Employee name \nPress 6 to get details of specific employee by entering Employee address");
            Console.WriteLine("Press 7 to get Total Salary of an employee \nPress any other key to exit");
            try
            {
            int input=Convert.ToInt32(Console.ReadLine());
            if(input!=1  && employeeList.Count<1 && input!=0 && input<=7  )
            {
                Console.WriteLine("No record!");
            }
            else
            {
                
                IsForDetail isForDetail;
                switch(input)
                {
                    case 1:
                    employeeList.Add(AddEmployee(employeeList));
                    Console.WriteLine("Employee Details Added\n");
                    break;

                    case 2:
                    isForDetail=new IsForDetail(AllDetails);
                    isForDetail(employeeList);
                    break;

                    case 3:
                    Console.WriteLine("Please enter the employee email address");
                    string updateByEmail=Console.ReadLine();
                    if(UpdateEmployee(employeeList,updateByEmail))
                    Console.WriteLine("Employee Detail Updated\n");
                    else
                    Console.WriteLine($"no record with email address {updateByEmail}\n");
                    break;

                    case 4:
                    Console.WriteLine("Please enter the employee email address");
                    string deleteByEmail=Console.ReadLine();
                    if(Delete(employeeList,deleteByEmail))
                    {
                        Console.WriteLine("Employee Details Deleted\n");
                    }                    
                    else
                    Console.WriteLine("No Employee Details Present\n");
                    break;

                    case 5:
                     Console.WriteLine("Please enter the employee first name");
                    string deleteByName=Console.ReadLine();
                    bool flag=false;
                    if(Delete(employeeList,deleteByName,flag))
                    {
                        Console.WriteLine($"All details of employees having first name as {deleteByName} is deleted\n");
                    }
                    else
                    Console.WriteLine("No Employee Details Present\n");
                    break;

                    case 6:
                    Console.WriteLine("Please enter the Employee Email address");
                    detailByEmail=Console.ReadLine();
                    isForDetail= new IsForDetail(DetailsByEmail);
                    isForDetail(employeeList);
                    break;

                    case 7:
                    Console.WriteLine("Please enter the Employee Email address");
                    string totalSalaryEmail=Console.ReadLine();
                    if(!TotalSalary(employeeList,totalSalaryEmail))
                    {
                        Console.WriteLine($"no employee with Email address {totalSalaryEmail}\n");
                    }
                    break;

                    default:
                    Console.WriteLine("*******Exiting*****");
                    Environment.Exit(1);
                    break;
                }
            }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                DisplayFormat(employeeList);
            }
            
        }
    }
    
}
